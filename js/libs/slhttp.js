class slHTTP {
    get(url) {
        const self = this;
        return new Promise(function(resolve, reject) {
            fetch(url)
            .then((res) => self.responseHelper(res))
            .then((data) => resolve(data))
            .catch((err) => reject(err));
        })
    }

    post(url, data, headers) {
        const self = this;
        return new Promise(function(resolve, reject) {
            fetch(url, {
                method: 'POST',
                headers,
                body: JSON.stringify(data)
            })
            .then((res) => self.responseHelper(res))
            .then((data) => resolve(data))
            .catch((err) => reject(err));
        })
    }
    put(url, data, headers) {
        const self = this;
        return new Promise(function(resolve, reject) {
            fetch(url, {
                method: 'PUT',
                headers,
                body: JSON.stringify(data)
            })
            .then((res) => self.responseHelper(res))
            .then((data) => resolve(data))
            .catch((err) => reject(err));
        })
    }  
    delete(url, headers) {
        const self = this;
        return new Promise(function(resolve, reject) {
            fetch(url, {
                method: 'DELETE',
                headers,
            })
            .then((res) => self.responseHelper(res))
            .then((data) => resolve({status: 200, message: "Resourse Deleted..."}))
            .catch((err) => reject(err));
        })
    }
    responseHelper(res) {
        if(res.ok)
            return res.json();
        throw new Error(`Some Issue: ${res.status} ${res.statusText}`);
    }
}

